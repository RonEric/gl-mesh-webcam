#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    glShadeModel(GL_FLAT);

    ofSetFrameRate(20);
    camWidth = 1280;  
    camHeight = 720;

    vidGrabber.setDeviceID(0);
    vidGrabber.setDesiredFrameRate(20);
    vidGrabber.initGrabber(camWidth, camHeight);

    

    
}

//--------------------------------------------------------------
void ofApp::update(){
    vidGrabber.update();

    pMesh  = new ofMesh();
    pMesh2 = new ofMesh();
    pMesh->enableIndices();
    pMesh2->enableIndices();
    pMesh2->enableColors();

    int factor = 10;
    int width  = 1280;
    int height = 720;

    ofPixels & pixels = vidGrabber.getPixels();
    for(size_t h = 0; h < height - 1; h+=factor){
        for(size_t w = 0; w < width - 1; w+=factor){

            int offset = h*width*3;

            int r = pixels[offset+w*3]; 
            int g = pixels[offset+w*3+1];
            int b = pixels[offset+w*3+2];
            int brightness = r + g + b / 3; 
            
            ofVec3f pos(w, h, brightness*0.2);
            pMesh->addVertex(pos);
            pMesh2->addVertex(pos);


            if(brightness > 0){
                ofColor c;
                int limit = std::max(std::max(r,g),b)-1;
                /*c.r = r > limit && g < limit && b < limit ? 200 : 0;
                c.g = r < limit && g > limit && b < limit ? 200 : 0;
                c.b = r < limit && g < limit && b > limit ? 200 : 0;
                */
               c.r = brightness + r > 255 ? 255 : brightness + r;
               c.g = brightness;
               c.b = brightness;
                pMesh->addColor(c);
                pMesh2->addColor(c);
            }
        }
    }
    width  = 1280;
    height = 720;
    height = height / factor;
    width  = width / factor;
    for(int h=0; h < height - 1; h++){
        for(int w=0; w < width - 1; w++){
            int a = h * width + w;
            int b = h * width + w + 1;
            
            int c = (h + 1) * width + w;
            int d = (h + 1) * width + w + 1;

            if(h % 2 == 0){
                if(w % 2 == 0){
                    pMesh2->addIndex(a);
                    pMesh2->addIndex(b);
                    //pMesh2->addIndex(a);
                   //pMesh2->addIndex(c);
                    pMesh2->addIndex(c);
                   //pMesh2->addIndex(b);

                    pMesh->addIndex(b);
                    pMesh->addIndex(c);
                    pMesh->addIndex(d);
                   // pMesh->addIndex(c);
                   // pMesh->addIndex(b);
                   // pMesh->addIndex(d);
                }   
                else{
                    pMesh2->addIndex(a);
                    pMesh2->addIndex(b);
                   // pMesh2->addIndex(a);
                    pMesh2->addIndex(d);
                    //pMesh2->addIndex(b);
                    //pMesh2->addIndex(d);  


                    pMesh->addIndex(a);
                    pMesh->addIndex(c);
                    pMesh->addIndex(d);
                    //pMesh->addIndex(c);
                    //pMesh->addIndex(a);
                    //pMesh->addIndex(d);
                }
        
            }
        else{
            if(w % 2 == 1){
                    pMesh2->addIndex(a);
                    pMesh2->addIndex(b);
                    //pMesh2->addIndex(a);
                    pMesh2->addIndex(c);
                    //pMesh2->addIndex(c);
                    //pMesh2->addIndex(b);

                    pMesh->addIndex(b);
                    pMesh->addIndex(c);
                    pMesh->addIndex(d);
                    //pMesh->addIndex(c);
                    //pMesh->addIndex(b);
                    //pMesh->addIndex(d);
                }   
                else{
                    pMesh2->addIndex(a);
                    pMesh2->addIndex(b);
                    //pMesh2->addIndex(a);
                    pMesh2->addIndex(d);
                    //pMesh2->addIndex(b);
                    //pMesh2->addIndex(d);  


                    pMesh->addIndex(a);
                    pMesh->addIndex(c);
                    pMesh->addIndex(d);
                    //pMesh->addIndex(c);
                    //pMesh->addIndex(a);
                    //pMesh->addIndex(d);
                }
            

            }


        }

    }
    
    vbo.setMesh(*pMesh, GL_DYNAMIC_DRAW);
    vbo2.setMesh(*pMesh2, GL_DYNAMIC_DRAW);
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(0);
    //vidGrabber.draw(0,0);
    vbo.drawElements(GL_TRIANGLES, 1280*720);
    vbo2.drawElements(GL_TRIANGLES ,1280*720);
    delete pMesh;
    delete pMesh2;

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
